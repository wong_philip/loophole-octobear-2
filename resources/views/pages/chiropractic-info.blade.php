@extends('app')

@section('title', 'Chiropractic and You')

@section('breadcrumbs', Breadcrumbs::render('chiropractic-info'))

@section('content')
    <h2>@yield('title')</h2>

    <div class="row">
        <div class="col-md-9 tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="vsc">
                @include('pages.chiropractic-info.vsc')
            </div>
            <div role="tabpanel" class="tab-pane fade" id="adjustment">
                @include('pages.chiropractic-info.adjustment')
            </div>
            <div role="tabpanel" class="tab-pane fade" id="children">
                @include('pages.chiropractic-info.children')
            </div>
            <div role="tabpanel" class="tab-pane fade" id="pregnancy">
                @include('pages.chiropractic-info.pregnancy')
            </div>
            <div role="tabpanel" class="tab-pane fade" id="back">
                @include('pages.chiropractic-info.back')
            </div>
            <div role="tabpanel" class="tab-pane fade" id="lowback">
                @include('pages.chiropractic-info.lowback')
            </div>
        </div>
        <div class="makeAGap hidden-lg"></div>
        <div class="col-md-3">
            <ul class="nav nav-pills nav-stacked" role="tablist" id="myTab">
                <li role="presentation" class="active">
                    <a href="#vsc" aria-controls="care" role="tab" data-toggle="pill">Vertebral Subluxation Complex</a>
                </li>
                <li role="presentation">
                    <a href="#adjustment" aria-controls="seminars" role="tab" data-toggle="pill">About the Adjustment</a>
                </li>
                <li role="presentation">
                    <a href="#children" aria-controls="seminars" role="tab" data-toggle="pill">Children and Chiropractic</a>
                </li>
                <li role="presentation">
                    <a href="#pregnancy" aria-controls="seminars" role="tab" data-toggle="pill">Pregnancy and Chiropractic</a>
                </li>
                <li role="presentation">
                    <a href="#back" aria-controls="seminars" role="tab" data-toggle="pill">Backpain and Chiropractic Care</a>
                </li>
                <li role="presentation">
                    <a href="#lowback" aria-controls="seminars" role="tab" data-toggle="pill">Three causes of low back pain</a>
                </li>
            </ul>
        </div>
    </div>
@endsection


@section('bottom-js')
    <script src="{{ asset('/assets/js/chiropractic-info.js') }}"></script>
@endsection