<h3>What to Except</h3>

<h4>Your First Visit</h4>
<p class="meet-our-team">
    When you first visit our office, you will be warmly welcomed by our staff. You will then be asked to fill out a form
    which will help us to know more about your health condition.
    Our Chiropractors will then meet with you and explain what chiropractors do, so that you will be better understand
    the specific questions regarding your condition that our chiropractors will ask to ascertain a provisional
    diagnosis. They will then perform the examination with some orthopaedic, neurological and chiropractic tests on you
    and we may request that you undergo X-rays to get further confirmation of your diagnosis.
</p>

<h4>Report of Findings</h4>
<p class="meet-our-team">
    After our Chiropractors have gathered all the important information from the consultation and the examination, they
    will give you a report of their findings and give their recommendations on how to increase your health level.
</p>

<h4>Adjustment</h4>
<p class="meet-our-team">
    Chiropractic adjustment is needed in order to reduce nerve interference to the body. The adjustments are very gentle
    and specific and our child patients loved it! Most of our patients begin to see results from even the very first
    adjustment.
</p>