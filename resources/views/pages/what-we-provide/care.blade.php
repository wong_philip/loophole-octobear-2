<h3>Chiropractic Care</h3>

<p class="lead">The following adjustment techniques are available in our practice.</p>

<h4>Manual Diversified</h4>
<p class="answer">
    The most commonly used adjustment technique by chiropractors. Diversified is characterized by a gentle
    high-velocity,
    low-amplitude thrust. Its objective is to restore proper movement and alignment of spine and joint dysfunction.
</p>

<h4>Thompson Drop</h4>
<p class="answer">
    Relies on a special table with sectional drop pieces that allow the chiropractor to use the
    patient's body weight when adjusting. It also places emphasis on strict procedural protocols.
</p>

<h4>Activator</h4>
<p class="answer">
    A light force application which is suitable for children to adults. The activator is a small handheld spring-loaded
    instrument which delivers a small impulse to the spine. It has been found to give off no more than 0.3 J of kinetic
    energy in a 3-millisecond pulse.
</p>

<h4>ArthroStim</h4>
<p class="answer">
    ArthroStim is an adjusting instrument that is an alternative to manual adjustments.
</p>

<h4>Flexion/Distraction</h4>
<p class="answer">
    A technique utilized for Lumbar Disc Herniation, in addition it is very effective for Lumbar muscular release. A
    gentle traction-ing force is applied to the spine, while the motor flexes the lower part of the table.
</p>

<h4>Shockwave Therapy</h4>
<p class="answer">
    Very effective treatment for muscular trigger point, tendonitis, tennis elbow, heel pain, frozen shoulder, and
    sports injury. This device is a combination of "radial shockwave," and "focus shockwave" technology.
</p>

<h4>Spinal Impulse</h4>
<p class="answer">
    Gentle, effective Spinal percussion device. This device is very similar to the activator, however it is motorized.
    It delivers a more uniform force, and our clients love this adjusting device!
</p>