<h3>Meet Our Team</h3>

<p class="meet-our-team">
    Our Clinic Chiropractors are graduates of The New Zealand College of Chiropractic and the
    Anglo-European College of Chiropractic in England which embrace the principles of the holistic,
    biopsychosocial approach to healthcare. The body is not a machine, it is a dynamic organ system that
    recuperates itself. With proper nutrition, good mental health, and a well-maintained spine, the body
    is able to exude health. Sunny Cheung and Ben Dyer are registered chiropractors with interests in
    prevention, function, and performance of the human body. They strive to maintain a state of wellness
    within each of their patients.
</p>

<p class="alert alert-info">
    Please click the picture of the doctor to view his details.
</p>

<div class="media">
    <div class="media-left media-top">
        <a id="doctor-pic-1">
            <img class="doctor" src="{{ asset('/assets/images/20121231051239_14696.jpg') }}"
                 title="Sunny Cheung">

        </a>
    </div>

    <div class="media-body">
        <h4 class="media-heading">Registered Chiropractor Sunny Cheung</h4>

        <div id="doctor1" hidden="hidden">
            <ul>
                <li>Bachelor of Chiropractic (New Zealand)</li>
                <li>Registered Chiropractor Australia</li>
                <li> Registered Chiropractor New Zealand</li>
                <li>Registered Chiropractor Hong Kong</li>
                <li>Member of Hong Kong Chiropractors Association</li>
            </ul>
        </div>
    </div>
</div>

<div class="media">
    <div class="media-left media-top">
        <a id="doctor-pic-2">
            <img class="doctor" src="{{ asset('/assets/images/20150313194606_56006.jpg') }}"
                 title="Ben Dyer">
        </a>
    </div>
    <div class="media-body">
        <h4 class="media-heading">Registered Chiropractor Ben Dyer</h4>

        <div id="doctor2" hidden="hidden">
            <ul>
                <li>Master of Chiropractic (AECC Bournemouth U.K.)</li>
                <li>Registered Chiropractor United Kingdom</li>
                <li>Registered Chiropractor Hong Kong</li>
                <li>Member of Hong Kong Chiropractors Association</li>
            </ul>
        </div>
    </div>
</div>

<div class="media">
    <div class="media-left media-top">
        <a id="doctor-pic-3">
            <img class="doctor" src="{{ asset('/assets/images/20121231051255_18654.jpg') }}"
                 title="Brendan Smallbone">
        </a>
    </div>
    <div class="media-body">
        <h4 class="media-heading">Registered Chiropractor Brendan Smallbone</h4>

        <p id="doctor3" class="doctor-info" hidden="hidden">
            In December 2014, Brendan Smallbone returned to his native New Zealand having spent 3 years
            with TSTCC. We would like to thank him for his hard work and commitment to improving the
            health
            of the residents of Hong Kong and wish him every success in his future endeavours.
        </p>
    </div>
</div>

<div class="media">
    <div class="media-left media-top">
        <a id="doctor-pic-4">
            <img class="doctor" src="{{ asset('/assets/images/Person-Male-Light-icon.png') }}"
                 title="Mike Davies">
        </a>
    </div>
    <div class="media-body">
        <h4 class="media-heading">Personal Trainer Mike Davies</h4>

        <div id="doctor4" hidden="hidden">
            <ul>
                <li>Advanced Personal Fitness Trainer Certification Course (AASFP)</li>
                <li>Master Trainer at the Hong Kong Football Club</li>
            </ul>
            <button class="btn btn-xs btn-link" id="doctor4-back"><h5>Background</h5></button>
            <blockquote>
                <p id="doctor4-backquote0">...</p>

                <p id="doctor4-backquote1" class="doctor-info" hidden="hidden">
                    My background is not one that typifies your conventional trainer
                    in that I am not overly qualified nor have I always been passionate about sport related activities
                    or
                    sport in general.
                    My accreditation from Fitness was more by way of graduating from the school of Life.
                    Like most, I was career bound to the office floor where I surrounded myself with
                    stress,
                    a misfit diet and an even more reckless schedule of late evenings. This path would
                    inevitably lead to tragedy so change was needed if not demanded.
                    Fitness was my saviour and it has not only led me to a better quality of life but
                    hopefully a longer and more fruitful one. My journey has been as rewarding as it has
                    been cathartic and I would be delighted to impart my knowledge to those who are
                    ready to
                    step up.
                </p>
            </blockquote>
        </div>
    </div>
</div>