<h3>About the Adjustment</h3>
<p class="lead">The Chiropractors can adjust the spine in different ways.</p>

<p>
    Sometimes the chiropractors will use their hands to deliver a fast, highly accurate thrust and sometimes they might
    use an instrument that directs a repeatable force to a fixated spinal joint.
</p>
<p>
    Other times, a slow, constant pressure is used. Some chiropractic doctors use special tables with moving sections.
    Depending on the need, several areas of the spine may be adjusted or just one.
</p>

<p>
    The key is to use the precise amount of energy at the correct spot, in the right direction, and at just the right
    time to get spinal joints moving again. Chiropractic is truly an art. Your chiropractic doctor has become a master
    at one or more adjusting techniques.
</p>
<p>
    Chiropractic is for everyone, including newborns, infants, children, seniors and even failed back surgery patients
    can benefit from chiropractic. Your chiropractor will tailor your chiropractic care according to your size, age and
    unique health problems.
</p>
<p>
    Chiropractic adjustments are specific, targeted, focused, and more accurate than spinal "manipulations" or
    "mobilization." It is this precision, combined with the purpose of reducing nervous system irritation, which has
    helped so many people. The purpose of chiropractic is to reduce the nervous system irritation, restoring your body
    to its optimal states.
</p>