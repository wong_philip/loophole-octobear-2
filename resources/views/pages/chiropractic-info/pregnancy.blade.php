<h3>Pregnancy and Chiropractic</h3>
<p class="lead">
    More and more new parents are beginning to trust the innate wisdom of the body. They are looking for a
    professional who can partner with them, supply information, coach, encourage and use natural approaches that honour
    both mother and baby.
    We would like to be that resource for you.
</p>

<h4>Pre-Conception</h4>
<p>
    Chiropractic care prior to conception promotes a more regular menstrual cycle and optimal uterine function. It
    prepares the body to be strong, supple and as balanced as possible to carry the pregnancy. Many couples who thought
    they were infertile have been helped by restoring proper nerve supply to reproductive organs.
</p>

<h4>Pregnancy</h4>
<p>
    Regular chiropractic care during pregnancy helps maintain balance, alignment and flexibility of the body. When the
    growing baby is comfortable, it can assume the optimal birthing position. The birthing process is much easier after
    having a chiropractic adjustment.
</p>

<h4>Labor and Birth</h4>
<p>
    Mothers who get adjusted seem to require less high-tech intervention. Most report shorter, less traumatic births.
    With proper nerve supply, well-timed contractions help move the baby more easily through the birth canal.
</p>

<h4>Post Delivery</h4>
<p>
    Even natural births can stress a baby's spine. Breastfeeding problems can arise if the baby is unable to comfortably
    turn its head. Make sure you have a chiropractic check-up for your newborn as soon as possible.
    You can relax in knowing we use a light touch to make our adjustments safe, comfortable and effective.
</p>