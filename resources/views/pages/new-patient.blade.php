@extends('app')

@section('title', 'New Patients')

@section('breadcrumbs', Breadcrumbs::render('new-patient'))

@section('content')
    <h2>@yield('title')</h2>

    <div class="row">
        <div class="col-md-9 tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="what-to-except">
                @include('pages.new-patient.what-to-except')
            </div>
            <div role="tabpanel" class="tab-pane fade" id="fees">
                @include('pages.new-patient.fees')
            </div>
            <div role="tabpanel" class="tab-pane fade" id="faq">
                @include('pages.new-patient.faq')
            </div>
        </div>
        <div class="makeAGap hidden-lg"></div>
        <div class="col-md-3">
            <ul class="nav nav-pills nav-stacked" role="tablist" id="myTab">
                <li role="presentation" class="active">
                    <a href="#what-to-except" aria-controls="care" role="tab" data-toggle="pill">What to Except</a>
                </li>
                <li role="presentation">
                    <a href="#fees" aria-controls="seminars" role="tab" data-toggle="pill">Fees</a>
                </li>
                <li role="presentation">
                    <a href="#faq" aria-controls="seminars" role="tab" data-toggle="pill">FAQ</a>
                </li>
            </ul>
        </div>
    </div>
@endsection


@section('bottom-js')
    <script src="{{ asset('/assets/js/new-patient.js') }}"></script>
@endsection