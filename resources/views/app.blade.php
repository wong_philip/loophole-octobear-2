<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        @yield('title') - Tsim Sha Tsui Chiropractic Centre
    </title>

    <link href="{{ asset('/assets/css/app.css')}} " rel="stylesheet">
    <link href="{{ asset('/assets/css/customize.css') }}" rel="stylesheet">
    @yield('css')

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    @yield('head-js')
</head>
<body>

@include('navbar')

<div class="container-fluid">
    @yield('breadcrumbs')

    @yield('content')
</div>

<footer class="footer">
    <div class="container-fluid">
        <p class="text-muted footerText">Copyright &copy; 2012 - 2015 Tsim Sha Tsui Chiropractic Centre</p>
    </div>
</footer>

<!-- Scripts -->
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="{{ asset('/assets/js/tabbed.js') }}"></script>
@yield('bottom-js')
</body>
</html>
