<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PagesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return String
	 */

    public function __construct()
    {
        $this->middleware('guest');
    }

	public function index()
	{
		return view('pages.index');
	}

    public function about()
    {
        return view('pages.about-us');
    }

    public function services()
    {
        return view('pages.what-we-provide');
    }

    public function news()
    {
        return view('pages.news');
    }

    public function patient()
    {
        return view('pages.new-patient');
    }

    public function chiropractic()
    {
        return view('pages.chiropractic-info');
    }

    public function contact()
    {
        return view('pages.contact-us');
    }

}
