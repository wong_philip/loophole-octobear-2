<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Static Pages Route, to prevent possible exceptions
Route::get('/', [
    'as' => 'home',
    'uses' => 'PagesController@index'
]);
Route::get('/about-us', [
    'as' => 'about-us',
    'uses' => 'PagesController@about'
]);
Route::get('/what-we-provide', [
    'as' => 'what-we-provide',
    'uses' => 'PagesController@services'
]);
Route::get('/news', [
    'as' => 'news',
    'uses' => 'PagesController@news'
]);
Route::get('/new-patient', [
    'as' => 'new-patient',
    'uses' => 'PagesController@patient'
]);
Route::get('/chiropractic-info', [
    'as' => 'chiropractic-info',
    'uses' => 'PagesController@chiropractic'
]);
Route::get('/contact-us', [
    'as' => 'contact-us',
    'uses' => 'PagesController@contact'
]);
//End Static Pages

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
