/**
 * Created by lawrencelaw on 28/3/15.
 */
$(function () {
    $("#navcare").click(function () {
        window.scrollTo(0, 0);
        $('#myTab a[href="#care"]').tab('show');
    });
    $("#navsemi").click(function () {
        window.scrollTo(0, 0);
        $('#myTab a[href="#seminars"]').tab('show');
    });
});