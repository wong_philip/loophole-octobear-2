$(function() {
    var url = document.location.toString();
    if (url.match('#')) {
        window.scrollTo(0, 0);
        $('.nav-pills a[href=#' + url.split('#')[1] + ']').tab('show');
    }

    $('.nav-pills a').on('shown.bs.tab', function (e) {
        window.scrollTo(0, 0);
        window.location.hash = e.target.hash;
    });
});